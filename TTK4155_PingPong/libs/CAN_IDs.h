#ifndef CAN_IDS_H_
#define CAN_IDS_H_

#define CAN_ID_BALL_DROPPED		9
#define CAN_ID_START_GAME		11
#define CAN_ID_STOP_GAME		13
#define CAN_ID_HW_READY			15
#define CAN_ID_JOYSTICK			100

#endif /* CAN_IDS_H_ */