/*
 * TTK4155_PingPong_SAM.c
 *
 * Created: 13.10.2022 09:54:34
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

//#define GREEN_BOX

#include "sam.h"
#include "libs/adc.h"
#include "libs/servo.h"
#include "libs/uart.h"
#include "libs/printf-stdarg.h"
#include "libs/can_controller.h"
#include "libs/can_interrupt.h"
#include "libs/timer.h"
#include "libs/actuator.h"
#include "../libs/CAN_IDs.h"

#include <stdint.h>

#define CAN_BR_SETTING	0x00530222

#ifdef GREEN_BOX
#define PHOTO_LOW_TH	200
#define PHOTO_HIGH_TH	1000
#else
#define PHOTO_LOW_TH	2000
#define PHOTO_HIGH_TH	2500
#endif

/************************************************************************/
/* FUNCTIONS                                                            */
/************************************************************************/

/**
 * @brief blocking delay function
 * @param cycles 	approx. number of cycles the function will block
 */
void _delay_cycles(uint32_t cycles) {
	uint32_t i;
	for (i=0;i<cycles;i++) {
		asm("nop");
	}
}

/************************************************************************/
/* MAIN                                                                 */
/************************************************************************/
int main(void) {
	uint8_t game_active = 1;
	uint16_t photocell;
    /* Initialize the SAM system */
    SystemInit();
    configure_uart();
	
	actuator_init();
	motor_reference();
	
	can_init(CAN_BR_SETTING, 1, 1);
	
	// disable WDT
	WDT->WDT_MR		= WDT_MR_WDDIS;
	
	init_timer();
	adc_init();
	
	CAN_MESSAGE message_control_data;
	
    /* Replace with your application code */
    while (1) {
		photocell = adc_read();
		if ((photocell < PHOTO_LOW_TH) && game_active) {
			game_active = 0;
			can_send_id_only(CAN_ID_BALL_DROPPED);
		} else if ((photocell > PHOTO_HIGH_TH) && !game_active) {
			can_send_id_only(CAN_ID_HW_READY);
			_delay_cycles(10000);
		}
		motor_get_position();
		
		if (g_new_message) {
			g_new_message = 0;
			switch (g_message_rx.id) {
				case CAN_ID_JOYSTICK:
					message_control_data = g_message_rx;
					break;
				case CAN_ID_STOP_GAME:
					game_active = 0;
					break;
				case CAN_ID_START_GAME:
					game_active = 1;
					break;
				default:
					break;
			} 
		}
		
		if (game_active) {
			set_servo(message_control_data.data[0]);

			if (message_control_data.data[2]){
				push_solenoid();
			} else {
				release_solenoid();
			}
			
			if (g_tick) {
				pid_controller(message_control_data.data[1], motor_get_position());
				g_tick = 0;
			}
		}
    }
}

