/*
 * servo.c
 *
 * Created: 21.10.2022 09:20:44
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#include "servo.h"
#include "timer.h"
#include "sam.h"

/**
 * @brief Set servo to position
 * @param position_percent Position in percent
 */
void set_servo(uint8_t position_percent) {
	if (position_percent <= 100) {
		REG_PWM_CDTY5 = 200 - position_percent;
	} else {
		REG_PWM_CDTY5 = 100;
	}
}

