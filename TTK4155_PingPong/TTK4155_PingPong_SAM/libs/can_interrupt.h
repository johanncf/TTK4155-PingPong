/*
 * can_interrupt.h
 *
 * Author: Gustav O. Often and Eivind H. J�lsgard
 *
 * For use in TTK4155 Embedded and Industrial Computer Systems Design
 * NTNU - Norwegian University of Science and Technology
 *
 */ 


#ifndef CAN_INTERRUPT_H_
#define CAN_INTERRUPT_H_
#include <stdint.h>
#include "can_controller.h"

void CAN0_Handler       ( void );

volatile CAN_MESSAGE g_message_rx;
volatile uint8_t g_new_message;

#endif /* CAN_INTERRUPT_H_ */