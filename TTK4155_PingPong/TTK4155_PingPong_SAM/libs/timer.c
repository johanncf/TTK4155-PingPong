/*
 * timer.c
 *
 * Created: 21.10.2022 09:20:08
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 
#include "timer.h"
#include "sam.h"

/**
 * @brief Initialize timer and PWM
 */
void init_timer() {
	PMC->PMC_PCER1	= PMC_PCER1_PID36;
	
	PIOC->PIO_OER	|= PIO_ABSR_P19;
	PIOC->PIO_PDR	|= PIO_PDR_P19;		// Disable PIO
	PIOC->PIO_ABSR	|= PIO_ABSR_P19;	// Peripheral B select
	
	PWM_WPCR_WPCMD(0);				// disable write protection
	PWM->PWM_CLK					= 0x000002D2;	//DIVA= 210    //PREA=MCK/4   //PREB = 0
	PWM->PWM_CH_NUM[5].PWM_CMR		= PWM_CMR_CPRE_CLKA | PWM_CMR_CPOL;	//0b1011
	PWM->PWM_CH_NUM[5].PWM_CPRD		= 2000;
	PWM->PWM_CH_NUM[5].PWM_CDTY		= 150;
	PWM->PWM_SCUC	= 0x01;
	PWM->PWM_ENA	= PWM_ENA_CHID5;
	
	NVIC_DisableIRQ(TC0_IRQn);
	
	PMC->PMC_PCER0	= PMC_PCER0_PID27;
	
	TC0->TC_WPMR				= TC_WPMR_WPKEY_PASSWD;
	TC0->TC_CHANNEL[0].TC_CMR	= TC_CMR_TCCLKS_TIMER_CLOCK3 | TC_CMR_CPCTRG;		// MCK/32
	TC0->TC_CHANNEL[0].TC_RC	= 2625;								// 1 kHz
	
	//Enable interrupt in NVIC 
	NVIC_ClearPendingIRQ(TC0_IRQn);
	NVIC_EnableIRQ(TC0_IRQn);
	
	TC0->TC_CHANNEL[0].TC_IER	= TC_IER_COVFS | TC_IER_CPCS ;
	TC0->TC_CHANNEL[0].TC_CCR	= TC_CCR_CLKEN | TC_CCR_SWTRG;
}

/**
 * @brief Timer interrupt handler
 */
void TC0_Handler (void) {
	/* reset interrupt flags */
	TC0->TC_CHANNEL[0].TC_SR;
	g_tick = 1;
}