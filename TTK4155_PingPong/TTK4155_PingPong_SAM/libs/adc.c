/*
 * adc.c
 *
 * Created: 21.10.2022 15:01:33
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#include "sam.h"
#include "adc.h"

/**
 * @brief Initialize internal ADC
 */
void adc_init(void) {
	PMC->PMC_PCER1	= ID_ADC;

	ADC->ADC_MR 	= ADC_MR_FREERUN_ON + ADC_MR_PRESCAL(1000) + ADC_MR_SETTLING_AST5;
	ADC->ADC_CHER 	= ADC_CHDR_CH0;
	ADC->ADC_CR 	= ADC_CR_START;
}

/**
 * @brief Read value from ADC
 * @return uint16_t 	value from ADC
 */
uint16_t adc_read() {
	return ADC->ADC_CDR[0];
}