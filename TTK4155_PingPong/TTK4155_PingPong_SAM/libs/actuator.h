/*
 * actuator.h
 *
 * Created: 10/28/2022 10:29:47 AM
 *  Author: maxla
 */ 
#ifndef ACTUATOR_H_
#define ACTUATOR_H_

/**
 * @brief Initialize actuators
 */
void actuator_init(void);

/**
 * @brief Enable output of solenoid
 */
void push_solenoid(void);

/**
 * @brief Disable output of solenoid
 */
void release_solenoid(void);

/**
 * @brief 
 * 
 * @param speed 
 */

/**
 * @brief Moves motor with given speed
 * @param speed     Speed of the motor
 */
void motor_move(int8_t speed);

// TODO: Both necessary???
//void motor_move_pos(uint16_t pos);

/**
 * @brief Reads encoder value of motor
 * @return uint16_t     encoder value
 */
uint16_t motor_get_position(void);

/**
 * @brief Moves motor to reference position and resets counter
 */
void motor_reference(void);

/**
 * @brief PID controller to move motor to desired position
 * @param p_set    setpoint
 * @param p_meas   measured position 
 */
void pid_controller(uint16_t p_set, uint16_t p_meas);
#endif /* ACTUATOR_H_ */