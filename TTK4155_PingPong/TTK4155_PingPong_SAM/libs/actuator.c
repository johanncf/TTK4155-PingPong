/*
 * actuator.c
 *
 * Created: 10/28/2022 10:28:45 AM
 *  Author: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#include <stdio.h>
#include <sam.h>
#include "actuator.h"
//#define DEBUG_ACTUATORS

#ifdef GREEN_BOX
	#define REF_MAX_LEFT	8854	// green box
	#define K_P				0.55
	#define K_I				0.22
	#define MAX_ERROR		40
	#define MAX_SUM_ERROR	150
#else
	#define REF_MAX_LEFT	1405	// blue box
	#define K_P				0.55
	#define K_I				0.07
	#define MAX_ERROR		70
	#define MAX_SUM_ERROR	200
#endif

#define PIO_SELENOID	PIOA

#define PIO_P_EN		PIO_PER_P9
#define PIO_P_DIR		PIO_PER_P10
#define PIO_P_OE		PIO_PER_P0
#define PIO_P_RST		PIO_PER_P1
#define PIO_P_SEL		PIO_PER_P2
#define PIO_ENCODER		PIOC
#define PIO_P_ALL		0xFF << 1

/************************************************************************/
/* PRIVATE                                                              */
/************************************************************************/

/**
 * @brief blocking delay function
 * @param cycles 	approx. number of cycles the function will block
 */
void delay_cycles(uint32_t cycles) {
	uint32_t i;
	for (i=0;i<cycles;i++) {
		asm("nop");
	}
}

/************************************************************************/
/* PUBLIC                                                               */
/************************************************************************/

/**
 * @brief Initialize actuators
 */
void actuator_init(void){
	PMC->PMC_PCER1	= PMC_PCER1_PID38;
	DACC->DACC_WPMR = DACC_WPMR_WPKEY(0x444143);// disable write protection
	DACC->DACC_MR	= DACC_MR_USER_SEL_CHANNEL1;
	DACC->DACC_CHER	= DACC_CHER_CH1;
	
	PMC->PMC_PCER0	= PMC_PCER0_PID11;
	PMC->PMC_PCER0	= PMC_PCER0_PID13;
	PMC->PMC_PCER0	= PMC_PCER0_PID14;
	
	PIO_SELENOID->PIO_PER	= (PIO_PER_P16);
	PIO_SELENOID->PIO_OER	= (PIO_PER_P16);
	PIO_SELENOID->PIO_IFDR	= (PIO_PER_P16);
	PIO_SELENOID->PIO_CODR	= (PIO_PER_P16);
	PIO_SELENOID->PIO_IDR	= (PIO_PER_P16);
	PIO_SELENOID->PIO_MDDR	= (PIO_PER_P16);
	PIO_SELENOID->PIO_PUDR	= (PIO_PER_P16);
	PIO_SELENOID->PIO_OWDR	= (PIO_PER_P16);
	
	PIO_ENCODER->PIO_PER	= (PIO_P_ALL);
	PIO_ENCODER->PIO_ODR	= (PIO_P_ALL);	// disable output
	PIO_ENCODER->PIO_IFDR	= (PIO_P_ALL);
	PIO_ENCODER->PIO_CODR	= (PIO_P_ALL);
	PIO_ENCODER->PIO_IDR	= (PIO_P_ALL);
	PIO_ENCODER->PIO_MDDR	= (PIO_P_ALL);
	PIO_ENCODER->PIO_PUDR	= (PIO_P_ALL);
	
	PIOD->PIO_PER	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_OER	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_IFDR	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_CODR	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_IDR	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_MDDR	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_PUDR	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	PIOD->PIO_OWDR	= (PIO_P_EN | PIO_P_DIR | PIO_P_OE | PIO_P_SEL | PIO_P_RST);
	
	delay_cycles(50000);
	PIOD->PIO_SODR	= (PIO_P_RST | PIO_P_EN); // release Reset again
}

/**
 * @brief Enable output of solenoid
 */
void push_solenoid (){
	PIO_SELENOID->PIO_CODR= (PIO_PER_P16);
}

/**
 * @brief Disable output of solenoid
 */
void release_solenoid (){
	PIO_SELENOID->PIO_SODR= (PIO_PER_P16);
}

/**
 * @brief Moves motor with given speed
 * @param speed     Speed of the motor
 */
void motor_move(int8_t speed) {

	PIOD->PIO_SODR = PIO_P_EN;
	if (speed < 0) {
		PIOD->PIO_CODR = PIO_P_DIR;
		speed = - speed;
	} else {
		PIOD->PIO_SODR = PIO_P_DIR;
	}

	DACC->DACC_CDR	= (uint16_t) speed<<6;
	
#ifdef DEBUG_ACTUATORS
	printf("speed: %d\n\r", speed);
#endif
}


//void motor_move_pos(uint16_t pos) {
//	uint16_t speed;
//#ifdef GREEN_BOX
//	pos = 100 - pos;
//#endif
//	PIOD->PIO_SODR	= (PIO_P_EN);
//	if (pos<=50) {
//		speed = 50 - pos;
//		PIOD->PIO_CODR= (PIO_P_DIR);
//	} else {
//		speed = pos - 50;
//		PIOD->PIO_SODR= (PIO_P_DIR);
//	}
//
//	DACC->DACC_CDR	= (uint16_t) speed<<6;
//#ifdef DEBUG_ACTUATORS
//	printf("speed: %d\n\r", speed);
//#endif
//}

/**
 * @brief Reads encoder value of motor
 * @return uint16_t     encoder value
 */
uint16_t motor_get_position(void) {
	uint16_t encoder_value;
	uint16_t normalized_position;
	PIOD->PIO_SODR = (PIO_P_RST);
	
	// get MSBs
	PIOD->PIO_CODR = (PIO_P_OE | PIO_P_SEL);
	delay_cycles(400000);
	encoder_value = (PIO_ENCODER->PIO_PDSR & PIO_P_ALL)<<7;
	// get LSBs
	PIOD->PIO_SODR = (PIO_P_SEL);
	delay_cycles(400000);
	encoder_value |= (PIO_ENCODER->PIO_PDSR & PIO_P_ALL)>>1;
		
	PIOD->PIO_SODR = (PIO_P_OE);
	
	normalized_position = (encoder_value * 100) / REF_MAX_LEFT;
#ifdef DEBUG_ACTUATORS
		printf("encoder:    %d\n\r", encoder_value);
		//printf("normalized: %d\n\r", normalized_position);
#endif
	return normalized_position;
}

/**
 * @brief Moves motor to reference position and resets counter
 */
void motor_reference(void) {
	motor_move(50);
	printf("Reference movement...\n\r");

	delay_cycles(8400000);
	delay_cycles(8400000);

	printf("Finished reference\n\r");
	motor_move(0);
	
	PIOD->PIO_CODR	= (PIO_P_RST); // set Reset
	delay_cycles(8400000);
	PIOD->PIO_SODR	= (PIO_P_RST); // release Reset again
	motor_get_position();
}

/**
 * @brief PID controller to move motor to desired position
 * @param p_set    setpoint
 * @param p_meas   measured position 
 */
void pid_controller(uint16_t p_set, uint16_t p_meas) {
	static int16_t sum_e = 0;
	int8_t speed_u, e;

	if		(p_set < 10) p_set = 10;
	else if (p_set > 90) p_set = 90;

	p_set = 100 - p_set;
	
	e = p_meas - p_set;
	
	if		(e < -MAX_ERROR) e = -MAX_ERROR;
	else if (e >  MAX_ERROR) e =  MAX_ERROR;

	if		((sum_e + e) >  MAX_SUM_ERROR)	sum_e =  MAX_SUM_ERROR;
	else if ((sum_e + e) < -MAX_SUM_ERROR)	sum_e = -MAX_SUM_ERROR;
	else									sum_e =  sum_e + e;
	
#ifdef DEBUG_ACTUATORS
	printf("p_meas: %d\n\r",p_meas);
	printf("p_set:  %d\n\r",p_set);
	printf("e:      %d\n\r",e);
#endif

	speed_u = K_P * e  +  K_I * sum_e;

#ifdef DEBUG_ACTUATORS
	printf("%04d; %04d; %04d\n\r", e, sum_e, speed_u);
#endif
	//printf("%04d; %04d; %04d; %04d; %04d\n\r", p_set, p_meas, e, sum_e, speed_u);

	motor_move(speed_u);
};
	
