/*
 * adc.h
 *
 * Created: 21.10.2022 15:01:42
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 


#ifndef ADC_H_
#define ADC_H_

/**
 * @brief Initialize internal ADC
 */
void adc_init(void);

/**
 * @brief Read value from ADC
 * @return uint16_t 	value from ADC
 */
uint16_t adc_read();



#endif /* ADC_H_ */