/*
 * servo.h
 *
 * Created: 21.10.2022 09:20:33
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 


#ifndef SERVO_H_
#define SERVO_H_

#include <stdint.h>

/**
 * @brief Set servo to position
 * @param position_percent Position in percent
 */
void set_servo(uint8_t position_percent);


#endif /* SERVO_H_ */