/*
 * timer.h
 *
 * Created: 21.10.2022 09:20:17 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 


#ifndef TIMER_H_
#define TIMER_H_
#include <stdint.h>

/* global timer interrupt flag */
volatile uint8_t g_tick;

/**
 * @brief Initialize timer and PWM
 */
void init_timer();

/**
 * @brief Timer interrupt handler
 */
void TC0_Handler(void);


#endif /* TIMER_H_ */