/*
 * uart.c
 *
 * Created: 02.09.2022 13:21:00
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 *
 * Driver library for the serial interface.
 */

#include "uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>

/* MACROS */
#define	UART_TX_BUSY    !(UCSR0A & (1<<UDRE0))
#define UART_RX_BUSY    !(UCSR0A & (1<<RXC0))

/************************************************************************/
/* PUBLIC                                                               */
/************************************************************************/

/**
 *	Initializes the USART module of the MCU for a given BAUD rate.
 */
void uart_init( unsigned int baud_rate ) {
	unsigned int ubrr_val;

	cli();

	ubrr_val = F_CPU/16/baud_rate-1;

	/* Set baud rate */
	UBRR0H = (unsigned char) (ubrr_val>>8);
	UBRR0L = (unsigned char) ubrr_val;
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8 data, 1 stop bit */
	UCSR0C = (1<<URSEL0)|(3<<UCSZ00);

	sei();

	/* Reroute fprint() to serial port */
	fdevopen(uart_send_byte, uart_receive_byte);
	
	/* clear screen */
	printf("\033[2J\r");
}


/**
 *	Send a single byte to the serial port.
 */
int uart_send_byte( char byte ) {
	/* Wait for empty transmit buffer */
	while ( UART_TX_BUSY );

	/* Put data into buffer, sends the data */
	UDR0 = byte;

	return 0;
}


/**
 *	Send a byte stream to the serial port.
 */
void uart_send ( char * source, unsigned int length ) {
	unsigned int i;

	for (i=0;i<length;i++) {
		uart_send_byte(source[i]);
	}
}


/**
 *	Check if a new byte was received from the serial bus.
 */
uint8_t uart_data_is_available ( void ) {
	return UART_RX_BUSY ? 1 : 0;
}


/**
 *	Read single byte from the serial bus.
 */
int uart_receive_byte ( void ) {
	/* Wait for data to be received */
	while ( UART_RX_BUSY );

	/* Get and return received data from input buffer */
	return UDR0;
}


/**
 *  Read data from the serial bus until no new bytes anymore or buffer is full.
 */
void uart_receive (char * destination, unsigned int lenght ) {
	unsigned int i = 0;

	/* Read received data from input buffer into the programm buffer */
	while ( (i<lenght) && uart_data_is_available() ){
		destination[i] = uart_receive_byte();
        i++;
	}
}