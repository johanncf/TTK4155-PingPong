/*
 * spi.c
 *
 * Created: 05.10.2022 17:00:00
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 
#include "spi.h"
#include <avr/io.h>

/**
 * @brief Initializes the SPI Transfer to Cn controller
 *        PDF:"Atmega Datasheet" page 160 
 */
void SPI_init(void) {
  /* Set MOSI and SCK output, all others input */
  DDR_SPI |= (1<<DD_MOSI)|(1<<DD_SCK);
  DDR_SPI &= ~(1<<DD_MISO);
  /* Enable SPI, Master, set clock rate fck/16 */
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
}

/**
 * @brief Read byte from SPI bus
 * @return char     received byte
 */
char SPI_read(){
	SPI_write(0x00);
	return SPDR;
}

/**
 * @brief Read byte to SPI bus
 * @param data Byte to transmit
 */
void SPI_write(uint8_t data){
	/* Start transmission */
	SPDR = data;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF)));
}

