/*
* oled.c
*
* Created: 23.09.2022 10:01:52
* Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
*/
#include "oled.h"
#include "fonts.h"
#ifndef F_CPU
#define F_CPU 4915200
#endif
#include <util/delay.h>
#include <avr/io.h>

volatile char * OLED_data = (char *) 0x00 ;
volatile char * OLED_cmd = (char *) 0x00 ;
e_font_size g_font_size = SMALL;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

/**
 *	Write command byte to the display
 */
void OLED_write_cmd(uint8_t cmd);


/************************************************************************/
/* PUBLIC                                                               */
/************************************************************************/

/**
 *  Initializes the OLED
 *  PDF:"OLED LY190-128064" section 9.4
 */
void OLED_init(char * data_address, char * cmd_address) {
	OLED_data = data_address;
	OLED_cmd = cmd_address;

	MCUCR |= (1 << SRE);  // enable XMEM
	SFIOR |= (1 << XMM2); // mask unused bits

	OLED_write_cmd(0xae); // display off
	OLED_write_cmd(0xa1); // segment remap
	OLED_write_cmd(0xda); // common pads hardware: alternative
	OLED_write_cmd(0x12);
	OLED_write_cmd(0xc8); // common output scan direction:com63~com0
	OLED_write_cmd(0xa8); // multiplex ration mode:63
	OLED_write_cmd(0x3f);
	OLED_write_cmd(0xd5); // display divide ratio/osc. freq. mode
	OLED_write_cmd(0x80);
	OLED_write_cmd(0x81); // contrast control
	OLED_write_cmd(0x50);
	OLED_write_cmd(0xd9); // set pre-charge period
	OLED_write_cmd(0x21);
	OLED_write_cmd(0x20); // set Memory Addressing Mode
	OLED_write_cmd(0x02);
	OLED_write_cmd(0xdb); // VCOM deselect level mode
	OLED_write_cmd(0x30);
	OLED_write_cmd(0xad); // master configuration
	OLED_write_cmd(0x00);
	OLED_write_cmd(0xa4); // out follows RAM content
	OLED_write_cmd(0xa6); // set normal display
	OLED_write_cmd(0xaf); // display on
	_delay_ms(200);
	OLED_reset();

	OLED_home();
}

/**
 *	Reset the OLED
 */
void OLED_reset(void) {
	uint8_t row, col;
	OLED_home();
	for (row=0; row<OLED_ROWS; row++) {
		OLED_write_cmd(0xb0 + row);
		for (col=0; col<OLED_COLS; col++) {
			OLED_write_data(0x00);
		}
	}
	OLED_home();
}

/**
 *	Set cursor to (0,0)
 */
void OLED_home(void) {
	OLED_write_cmd(0xb0);	// page
	OLED_write_cmd(0x00);	// column LSB
	OLED_write_cmd(0x10);	// column MSB
}

/**
 *	Set cursor to (0, line)
 */
void OLED_goto_line(uint8_t line) {
	OLED_write_cmd(0xb0 + line);
	OLED_write_cmd(0x00);	// column LSB
	OLED_write_cmd(0x10);	// column MSB
}

/**
 *	clear given line
 */
void OLED_clear_line(uint8_t line) {
	uint8_t i;
	OLED_goto_line(line);

	for (i=0; i<OLED_COLS; i++) {
		OLED_write_data(0x00);
	}
}

/**
 *	Set cursor to (column, row)
 */
void OLED_pos(uint8_t row, uint8_t column) {
	OLED_goto_line(row);
	OLED_write_cmd(0x00 | (column & 0x0F));			// column LSB
	OLED_write_cmd(0x10 | ((column & 0xF0)>>4));	// column MSB
}

/**
 *	Write 8 pixels to the OLED at the current cursor position
 */
void OLED_write_data(uint8_t data) {
	//xmem_write(data, OLED_DATA_ADDRESS);
	if (OLED_data) {
		*OLED_data = data;
	}
}

/**
 *	Write string to the OLED
 */
int OLED_print(char* text) {
	int i=0;
	char next_char = text[i];
	int k, k_max = 4;
	uint8_t byte;

	switch(g_font_size) {
		case MID: k_max = 5; break;
		case BIG: k_max = 8; break;
		default: k_max = 4; break;
	}

	while (next_char){
		for(k=0;k<k_max;k++){
			switch(g_font_size) {
				case MID: byte  = pgm_read_byte(&(font5[next_char-32][k])); break;
				case BIG: byte  = pgm_read_byte(&(font8[next_char-32][k])); break;
				default: byte  = pgm_read_byte(&(font4[next_char-32][k])); break;
			}
		OLED_write_data(byte);
		}
		i++;
		next_char = text[i];
	}
	
	return 0;
}

/**
 *	Write number to the OLED
 */
int OLED_print_int(int16_t number, e_alignment alignment) {
	char int_string[7] = {0,0,0,0,0,0,0};
	uint8_t i = 0;
	uint16_t divider = 10000;
	uint16_t digit;
	
	if (number < 0) {
		int_string[i++] = '-';
		number = - number;
	}

	while (divider) {
		digit = number / divider;
		divider /= 10;
		switch (alignment) {
			case LEFT_ALIGN:
				if (digit) int_string[i++] = digit + 48;
				break;
			case RIGHT_ALIGN:
				if (digit) int_string[i++] = digit + 48;
				else	   int_string[i++] = ' ';
				break;
			case LEADING_ZERO:
				int_string[i++] = digit + 48;
				break;
			default: break;
		}
	}
	
	OLED_print(int_string);
	
	return 0;
}

/**
 *	Set font size
 */
void OLED_set_font_size(e_font_size size) {
	g_font_size = size;
}

/**
 *	Set brightness to lvl
 */
void OLED_set_brightness(uint8_t lvl) {
	OLED_write_cmd(0x81); //contrast control
	OLED_write_cmd(lvl);
}

/************************************************************************/
/* PRIVATE                                                              */
/************************************************************************/

/**
 *	Write command byte to the display
 */
void OLED_write_cmd(uint8_t cmd) {
	if (OLED_cmd) {
		*OLED_cmd = cmd;
	}
}