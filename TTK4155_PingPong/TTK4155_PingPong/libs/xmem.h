/*
 * xmem.h
 *
 * Created: 09.09.2022 10:18:48
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 


#ifndef XMEM_H_
#define XMEM_H_

#include <stdint.h>

/**
 * @brief Initializes the external addressing module of the MCU.
 */
void xmem_init( void );

/**
 * @brief Write a single byte to ext. SRAM.
 * @param data  data to write to RAM
 * @param addr  address to write to
 */
void xmem_write( uint8_t data , uint16_t addr );

/**
 * @brief Read a single byte from ext. SRAM.
 * @param addr      address to read from
 * @return uint8_t  data from RAM
 */
uint8_t xmem_read( uint16_t addr );


/**
 * @brief Perform a self test on the SRAM.
 * (C) Copied from the provided materials of the course. 
 */
void SRAM_test(void);

#endif /* XMEM_H_ */