/*
 * spi.h
 *
 * Created: 23.09.2022 10:02:03
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 
#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>

#define DD_MOSI DDB5 
#define DD_MISO DDB6
#define DDR_SPI DDRB 
#define DD_SS   PB4 
#define DD_SCK  PB7

/**
 * @brief Initializes the SPI Transfer to Cn controller
 *        PDF:"Atmega Datasheet" page 160 
 */
void SPI_init(void);

/**
 * @brief Read byte from SPI bus
 * @return char     received byte
 */
char SPI_read();

/**
 * @brief Read byte to SPI bus
 * @param data Byte to transmit
 */
void SPI_write(uint8_t data);

#endif /* SPI_H_ */