/*
 * can.h
 *
 * Created: 10/7/2022 11:28:42 AM
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 
#include <avr/io.h>  

#ifndef CAN_H_
#define CAN_H_

/* Configuratioon of the */
#define CAN_INT_PIN		PD2
#define CAN_DDR			DDRD

/* Global constants */
#define	RX_REG0			0
#define	RX_REG1			1
#define CAN_NORMAL		0
#define CAN_LOOPBACK 	1

/* Global typedefs */
typedef struct {
	uint16_t id ;
	char data_length ;
	char data[8];
} can_message;

/**
 *	Initializes the CAN controller
 */
void can_init(uint8_t loopback_enable);

/**
 *	Send a complete CAN message
 */
void can_message_send (can_message *msg);

/**
 *	Read a CAN message from the CAN controller into the MCU
 *  (only called in ISR)
 */
void can_data_receive(can_message *msg, uint8_t rx_reg);

/**
 *	Returns nonzero value if new message is available
 */
uint8_t can_new_message_received(void);

/**
 *	Returns latest CAN message
 */
can_message can_get_latest_message(void);

/**
 *	Send an empty CAN message with an ID only
 */
void can_send_id_only(uint16_t id);

ISR(INT0_vect);

#endif /* CAN_H_ */