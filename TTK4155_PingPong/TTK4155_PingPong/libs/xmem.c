/*
 * xmem.c
 *
 * Created: 09.09.2022 10:19:00
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#include "xmem.h"
#include "board_config.h"
#include <stdio.h>
#include <avr/io.h>
#include <stdlib.h>

/**
 * @brief Initializes the external addressing module of the MCU.
 */
void xmem_init( void ) {
	MCUCR |= (1 << SRE ); // enable XMEM
	SFIOR |= (1 << XMM2 ); // mask unused bits
}

/**
 * @brief Write a single byte to ext. SRAM.
 * @param data  data to write to RAM
 * @param addr  address to write to
 */
void xmem_write( uint8_t data , uint16_t addr ) {
	volatile char * ext_mem = ( char *) SRAM_BASE_ADDRESS ;
	ext_mem[addr] = data ;
}

/**
 * @brief Read a single byte from ext. SRAM.
 * @param addr      address to read from
 * @return uint8_t  data from RAM
 */
uint8_t xmem_read( uint16_t addr ) {
	volatile char * ext_mem = (char *) SRAM_BASE_ADDRESS ;
	uint8_t ret_val = ext_mem [addr];
	return ret_val ;
}

/**
 * @brief Perform a self test on the SRAM.
 * (C) Copied from the provided materials of the course. 
 */
void SRAM_test(void)
{
	volatile char *ext_ram = (char *) SRAM_BASE_ADDRESS; // Start address for the SRAM
	uint16_t ext_ram_size = SRAM_SIZE;
	uint16_t write_errors = 0;
	uint16_t retrieval_errors = 0;
	printf("Starting SRAM test...\r\n");
	// rand() stores some internal state, so calling this function in a loop will
	// yield different seeds each time (unless srand() is called before this function)
	uint16_t seed = rand();
	// Write phase: Immediately check that the correct value was stored
	srand(seed);
	for (uint16_t i = 0; i < ext_ram_size; i++) {
		uint8_t some_value = rand();
		ext_ram[i] = some_value;
		uint8_t retrieved_value = ext_ram[i];
		if (retrieved_value != some_value) {
			printf("Write phase error: ext_ram[%4d] = %02X (should be %02X)\r\n", i, retrieved_value, some_value);
			write_errors++;
		}
	}
	// Retrieval phase: Check that no values were changed during or after the write phase
	srand(seed);
	// reset the PRNG to the state it had before the write phase
	for (uint16_t i = 0; i < ext_ram_size; i++) {
		uint8_t some_value = rand();
		uint8_t retrieved_value = ext_ram[i];
		if (retrieved_value != some_value) {
			printf("Retrieval phase error: ext_ram[%4d] = %02X (should be %02X)\r\n", i, retrieved_value, some_value);
			retrieval_errors++;
		}
	}
	printf("SRAM test completed with \r\n%4d errors in write phase and \r\n%4d errors in retrieval phase\r\n\r\n", write_errors, retrieval_errors);
}