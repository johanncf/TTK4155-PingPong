/*
 * uart.h
 *
 * Created: 02.09.2022 13:20:28
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 *
 * Driver library for the serial interface.
 */


#ifndef UART_H_
#define UART_H_

#include <stdio.h>
#include "board_config.h"

/**
 *	Initializes the USART module of the MCU for a given BAUD rate.
 */
void uart_init( unsigned int baud_rate );

/**
 *	Send a single byte to the serial port.
 */
int uart_send_byte( char byte );

/**
 *	Send a byte stream to the serial port.
 */
void uart_send ( char * source, unsigned int length );

/**
 *	Check if a new byte was received from the serial bus.
 */
uint8_t uart_data_is_available ( void );

/**
 *	Read single byte from the serial bus.
 */
int uart_receive_byte( void );

/**
 *  Read data from the serial bus until no new bytes anymore or buffer is full.
 */
void uart_receive (char * destination, unsigned int lenght );

#endif /* UART_H_ */