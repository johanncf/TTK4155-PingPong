/*
 * mcp2515.c
 *
 * Created: 05.10.2022 17:00:00
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#include "mcp2515.h"
#include "spi.h"
#include <avr/io.h>

/**
 * @brief Initialize the MCP2515 CAN controller
 * @return uint8_t 	nonzero if something went wrong
 */
uint8_t mcp2515_init() {
	DDRB |= (1<<CAN_CS); 	// CS pin as output
	SPI_init(); 			// Initialize SPI
	mcp2515_reset(); 		// Send reset-command
	// Self-test
	if ((mcp2515_read(MCP_CANSTAT) & MODE_MASK) != MODE_CONFIG) {
		printf("MCP2515 is NOT in configuration mode after reset!\n\r"); 
		return 1;
	}
	return 0; 
}

/**
 * @brief Read value form register on MCP2515
 * @param address 	address of register to read
 * @return uint8_t 	register value
 */
uint8_t mcp2515_read(uint8_t address) {
	uint8_t result;
	PORTB &= ~(1<<CAN_CS);	// Select CAN-controller
	SPI_write(MCP_READ);	// Send read instruction SPI_write(address); // Send address
	SPI_write(address);
	result = SPI_read();	// Read result
	PORTB |= (1<<CAN_CS);	// Deselect CAN-controller
	return result;
}

/**
 * @brief Write bytes to register(s) on the can controller
 * @param address 	address of register
 * @param data 		byte-array to transfer to controller
 * @param length 	number of bytes to transfer
 */
void mcp2515_write(uint8_t address, uint8_t * data, uint8_t length) {
	PORTB &= ~(1<<CAN_CS);	// Select CAN-controller
	SPI_write(MCP_WRITE);	// Send write instruction SPI_write(address); // Send address
	SPI_write(address);		// Send read instruction SPI_write(address); // Send address
	int i;
	for(i = 0; i<length; i++){
		SPI_write(data[i]);
	}
	PORTB |= (1<<CAN_CS);	// Deselect CAN-controller
}

/**
 * @brief Software reset of the MCP2515
 */
void mcp2515_reset(){
	PORTB &= ~(1<<CAN_CS);	// Select CAN-controller
	SPI_write(MCP_RESET);	// Send write instruction SPI_write(address); // Send address
	PORTB |= (1<<CAN_CS);	// Deselect CAN-controller
}

/**
 * @brief Read interrupt flags of the MCP2515
 * @return uint8_t 	Content of the CANINTF register
 */
uint8_t mcp2515_read_status() {
	//return mcp2515_read(MCP_READ_STATUS);
	return mcp2515_read(MCP_CANINTF);
}

/**
 * @brief Request the transmission of the mailbox
 */
void mcp2515_request_to_send() {
	PORTB &= ~(1<<CAN_CS);	// Select CAN-controller
	SPI_write(MCP_RTS_TX0); // Send RTS flag
	PORTB |= (1<<CAN_CS);	// Deselect CAN-controller
}

/**
 * @brief Manipulate individual bits on a register on the MCP2515
 * @param address 	address of the register to manipulate
 * @param mask 		bitmask of the bits to change
 * @param newBits 	desired values of the masked bits
 */
void mcp2515_bit_modify(uint8_t address, uint8_t mask, uint8_t newBits) {
	PORTB &= ~(1<<CAN_CS);	// Select CAN-controller
	SPI_write(MCP_BITMOD); 	// Send bit modify instruction
	SPI_write(address);
	SPI_write(mask);
	SPI_write(newBits);
	PORTB |= (1<<CAN_CS);	// Deselect CAN-controller
}