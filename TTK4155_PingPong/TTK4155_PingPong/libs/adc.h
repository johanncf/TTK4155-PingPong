/*
 * adc.h
 *
 * Created: 16.09.2022 10:28:43
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 


#ifndef ADC_H_
#define ADC_H_

#include "board_config.h"


/**
 *	Initializes the ext. ADC
 */
void adc_init (void);

/**
 *	Read analog value from the given channel of the ext. ADC
 */
uint8_t adc_read(uint8_t channel);

/**
 *Read analog values of all channels from the ext. ADC
 */
void adc_read_all(uint8_t * ret_vals);

#endif /* ADC_H_ */