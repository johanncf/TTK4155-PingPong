/*
 * hmi.c
 *
 * Created: 16.09.2022 14:47:04
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 


#include "board_config.h"
#include "game.h"
#include "adc.h"
#include "oled.h"
#include "can.h"
#include "../../libs/CAN_IDs.h"
#include <avr/io.h>
#include <stdio.h>

#define JS_TRIG_UPPER 90
#define JS_TRIG_LOWER 5

static uint8_t min_x = 0x00;
static uint8_t max_x = 0xFF;
static uint8_t min_y = 0x00;
static uint8_t max_y = 0xFF;

/* local typedefs */
typedef enum pos_t { LEFT, RIGHT, UP, DOWN, NEUTRAL } pos_t;
typedef struct joystick_t {uint8_t x; uint8_t y; uint8_t button;} joystick_t;
typedef struct slider_t {uint8_t left; uint8_t right;} slider_t;
typedef struct buttons_t {uint8_t left; uint8_t right;} buttons_t;
typedef struct menu_point_t {
	struct menu_point_t *parent; 
	char name[12]; 
	void (*fnct_ptr)();
	struct menu_point_t *children;
} menu_point_t;


/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/
/* MENU & OLED */
void show_main_menu(void);
void fnct_score_menu(void);
void fnct_play_menu(void);
void fnct_calib_menu(void);
void print_headline(char * text);
uint8_t select_line(uint8_t start, uint8_t low, uint8_t high);

/* JOYSTICK: */
void joystick_calibrate(void);
pos_t joystick_pos_read_discrete(void);
void joystick_pos_read_perc(joystick_t * joystick);
void joystick_button_read(joystick_t * joystick);
void wait_for_joystick_pressed(void);

/* SLIDER: */
void slider_pos_read_perc(slider_t * slider);
void slider_button_read(buttons_t * buttons);
void ui_controls_send_can(void);


/* Menu structure */
menu_point_t main_menu = {
	.parent		= 0,
	.name		= "Main",
	.fnct_ptr	= 0
};

menu_point_t score_menu = {
	.parent		= &main_menu,
	.name		= "Score",
	.fnct_ptr	= fnct_score_menu
};

menu_point_t play_menu = {
	.parent		= &main_menu,
	.name		= "Play",
	.fnct_ptr	= fnct_play_menu
};

menu_point_t calib_menu = {
	.parent		= &main_menu,
	.name		= "Calibration",
	.fnct_ptr	= fnct_calib_menu
};


menu_point_t main_children[3];


/************************************************************************/
/* PUBLIC                                                               */
/************************************************************************/
void init_game(uint8_t calibrate_joystick) {
	main_children[0] = score_menu;
	main_children[1] = play_menu;
	main_children[2] = calib_menu;
	main_menu.children = main_children;

	/* OLED */
	OLED_init((char*) OLED_DATA_ADDRESS, (char*) OLED_CMD_ADDRESS);

	/* ANALOG FRONTEND */
	adc_init();
	
	if (calibrate_joystick) {
		joystick_calibrate();
	}

	/* BUTTONS */	
	/* use PB0, PB1 and PB2 as input */
	DDRB &= ~(7<< PB0);
	
	/* enable pull-up for PB2 */
	PORTB |= (1<< PB2);

	can_init(CAN_NORMAL);
}

/**
 *	Entry point to hmi state machine
 */
void start_game(){
	show_main_menu();
}


/************************************************************************/
/* Framework for user interface (PRIVATE)                               */
/************************************************************************/

/**
 *	Entry point to hmi state machine
 */
void show_main_menu(void) {
	print_headline(main_menu.name);
	OLED_goto_line(1);
	OLED_print("use joystick to");
	OLED_goto_line(2);
	OLED_print("decide next menu");
	OLED_goto_line(3);

	OLED_print("  ");
	OLED_print(main_menu.children[0].name);
	OLED_goto_line(4);
	OLED_print("  ");
	OLED_print(main_menu.children[1].name);
	OLED_goto_line(5);
	OLED_print("  ");
	OLED_print(main_menu.children[2].name);

	uint8_t selected_line = select_line(3, 0, 2);
	main_menu.children[selected_line].fnct_ptr();
}


/**
 *  Menu: game scores
 */
void fnct_score_menu(){
	print_headline(score_menu.name);
	OLED_goto_line(1);
	OLED_print("#1: ABC");
	OLED_goto_line(2);
	OLED_print("#2: DUMMY");
	OLED_goto_line(3);
	OLED_print("#3: DUMMY2");
	OLED_goto_line(7);
	OLED_print("Press joystick to exit.");

	_delay_ms(200);
	wait_for_joystick_pressed();
	show_main_menu();
}


/**
 *  Menu: play game
 */
void fnct_play_menu(){
	can_message message;
	uint8_t game_loaded = 1;
	uint8_t run_game = 1;
	int8_t lifes_left = 5;
	print_headline(play_menu.name);
	OLED_goto_line(3);
	
	OLED_set_font_size(BIG);
	OLED_print("   Lifes: ");
	OLED_print_int(lifes_left, LEFT);
	
	// Wake up the ATSAM controller
	can_send_id_only(CAN_ID_START_GAME);

	while (game_loaded) {
		if (run_game) {
			ui_controls_send_can();
		}

		_delay_ms(5);
		
		if (can_new_message_received()) {
			message = can_get_latest_message();
			switch (message.id) {
				case CAN_ID_BALL_DROPPED:
					lifes_left--;
					run_game = 0;
					
					OLED_goto_line(3);
					if (lifes_left <= 0) {
						OLED_print(" << GAME OVER >>");
						printf("GAME OVER!!\n\r");
						game_loaded = 0;
						can_send_id_only(CAN_ID_STOP_GAME);
					} else {
						OLED_print("   Lifes: ");
						printf("ball dropped; lifes left: %d\n\r", lifes_left);
						OLED_print_int(lifes_left, LEFT_ALIGN);
					}
					break;
					
				case CAN_ID_HW_READY:
					can_send_id_only(CAN_ID_START_GAME);
					printf("Send Start game\n\r");
					_delay_ms(100);
					run_game = 1;
					break;
					
				default: break;
			}
		}
	}
	
	OLED_goto_line(7);
	OLED_set_font_size(SMALL);
	OLED_print("Press joystick to exit.");
	_delay_ms(200);
	wait_for_joystick_pressed();
    show_main_menu();
}


/**
 *  Menu: calibrate joystick
 */
void fnct_calib_menu(){
	print_headline(calib_menu.name);
	OLED_goto_line(1);
	OLED_print("Please move the slider in a");
	OLED_goto_line(2);
	OLED_print("maximum range circle within"); 
	OLED_goto_line(3);
	OLED_print("the next 2 seconds.");
	OLED_goto_line(4);

	_delay_ms(2000);

	OLED_print("Start now!");

	joystick_calibrate();

	show_main_menu();
}


/**
 *  Print a headline on the OLED (clears screen before)
 */
void print_headline(char * text) {
	OLED_reset();
	OLED_set_font_size(BIG);
	OLED_print(text);
	OLED_set_font_size(SMALL);
}


/**
 *	Set a cursor to line [start + low] to [start + high] controlled by
 *	the joystick and returns the line number [low:high] if the joystick 
 *	was pressed.
 */
uint8_t select_line(uint8_t start, uint8_t low, uint8_t high) {
	pos_t joystick_read = joystick_pos_read_discrete();
	joystick_t joystick;
	uint8_t unlock_line_change = 1;
	int8_t selected_line = 0, last_selected_line = 1;
	joystick_button_read(&joystick);

	// check if the button was already pressed.
	if (joystick.button == 1) {
		while(joystick.button == 1) {
			joystick_button_read(&joystick);
		}
	}
	while(joystick.button == 0){
		joystick_read = joystick_pos_read_discrete();

		switch(joystick_read){
			case UP:
				if (unlock_line_change) selected_line--; 
				break;
			case DOWN:
				if (unlock_line_change) selected_line++; 
				break;
			case NEUTRAL: 
				unlock_line_change = 1;
				break;
			default:
				break;
		}
		if (selected_line < low) selected_line = low;
		else if (selected_line > high) selected_line = high;

		if (last_selected_line != selected_line) {
			OLED_goto_line(last_selected_line + start);
			OLED_print(" ");
			OLED_goto_line(selected_line + start);
			OLED_print(">");
			unlock_line_change = 0;
			
			last_selected_line = selected_line;
			_delay_ms(200);
		}
		joystick_button_read(&joystick);
	}

	return selected_line;
}

/************************************************************************/
/* Joystick                                                             */
/************************************************************************/

/**
 *	Calibrate min. and max. positions of the cursor
 */
void joystick_calibrate(void) {
	printf("CALIBRATION: Please move the slider in a maximum range\r\n");
	printf("             circle within the next 2 seconds.\r\n");
	uint8_t values[4];
	uint16_t cycle_counter;

	min_x = 0xFF;
	max_x = 0x00;
	min_y = 0xFF;
	max_y = 0x00;
	
	for (cycle_counter = 0; cycle_counter < UINT16_MAX; cycle_counter++) {
		adc_read_all(values);
		/* X-direction */
		if		(max_x < values[2]) max_x = values[2];
		else if (min_x > values[2]) min_x = values[2];
		
		/* Y-direction */
		if		(max_y < values[3]) max_y = values[3];
		else if (min_y > values[3]) min_y = values[3];
	}
	
	printf("CALIBRATION: min X: %d, max X: %d\r\n", min_x, max_x);
	printf("CALIBRATION: min Y: %d, max Y: %d\r\n", min_y, max_y);
}


/**
 *	Read the cursor positions in discrete values
 */
pos_t joystick_pos_read_discrete(void) {
	joystick_t js;
	pos_t ret_val;
	joystick_pos_read_perc(&js);
	
	if		(js.x >= JS_TRIG_UPPER && JS_TRIG_LOWER < js.y && js.y < JS_TRIG_UPPER ) ret_val = RIGHT;
	else if (js.x <= JS_TRIG_LOWER && JS_TRIG_LOWER < js.y && js.y < JS_TRIG_UPPER ) ret_val = LEFT;
	else if (JS_TRIG_LOWER < js.x && js.x < JS_TRIG_UPPER && js.y >= JS_TRIG_UPPER ) ret_val = UP;
	else if (JS_TRIG_LOWER < js.x && js.x < JS_TRIG_UPPER && js.y <= JS_TRIG_LOWER ) ret_val = DOWN;
	else ret_val = NEUTRAL;
	
	return ret_val;
}


/**
 *	Read the joystick positions in percent
 */
void joystick_pos_read_perc(joystick_t * joystick) {
	uint8_t values[4];
	adc_read_all(values);
	uint16_t x_pow_2 = values[2]*values[2];
	uint16_t y_pow_2 = values[3]*values[3];
	joystick->x = 0.00105*x_pow_2 + 0.1234*values[2];
	joystick->y = 0.00105*y_pow_2 + 0.1234*values[3];
	//joystick->x = (values[2] - min_x) * 100 / (max_x - min_x);
	//joystick->y = (values[3] - min_y) * 100 / (max_y - min_y);
}


/**
 *	Read the joystick button
 */
void joystick_button_read(joystick_t * joystick) {	
	joystick->button = (PINB & (1<<PB2)) ? 0 : 1;
}


/**
 *  Wait for the joystick to be pressed
 */
void wait_for_joystick_pressed(void) {
	joystick_t joystick;
	joystick_button_read(&joystick);
	if (joystick.button == 1) {
		while(joystick.button == 1) {
			joystick_button_read(&joystick);
		}
	}
	while(joystick.button == 0) {
		joystick_button_read(&joystick);
	}
}


/************************************************************************/
/* SLIDER                                                               */
/************************************************************************/
/**
 *	Read the slider positions in percent
 */
 void slider_pos_read_perc(slider_t * slider) {
	 uint8_t values[4];
	 adc_read_all(values);
	slider->left  = values[0] * 100 / 255;
	slider->right = values[1] * 100 / 255;
}


/**
 *	Read the touch buttons
 */
 void slider_button_read(buttons_t * buttons) {
	buttons->left = (PINB & (1<<PB0)) ? 1 : 0;
	buttons->right = (PINB & (1<<PB1)) ? 1 : 0;
}


/**
 *	Collect all game controller properties and send via CAN
 */
void ui_controls_send_can(void) {
	static joystick_t joystick;
	static slider_t	slider;
	static can_message message;
	joystick_pos_read_perc(&joystick);
	slider_pos_read_perc(&slider);
	joystick_button_read(&joystick);
	message.id = CAN_ID_JOYSTICK;
	message.data_length = 3;
	message.data[0] = joystick.x;
	
#ifdef USE_SLIDER_FOR_MOTOR
	message.data[1] = slider.right;
#else
	message.data[1] = joystick.y;
#endif /*USE_SLIDER_FOR_MOTOR*/
	
	message.data[2] = joystick.button;	
	can_message_send(&message);
}