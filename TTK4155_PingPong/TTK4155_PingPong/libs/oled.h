/*
 * oled.h
 *
 * Created: 23.09.2022 10:02:03
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#ifndef OLED_H_
#define OLED_H_

#include <stdint.h>

#define OLED_ROWS 8
#define OLED_COLS 128

typedef enum e_alignment {LEFT_ALIGN, RIGHT_ALIGN, LEADING_ZERO} e_alignment;
typedef enum e_font_size {SMALL, MID, BIG} e_font_size;

/**
 *	Initializes the OLED
 *  PDF:"OLED LY190-128064" section 9.4
 */
void OLED_init(char * data_address, char * cmd_address); 

/**
 *	Reset the OLED
 */
void OLED_reset(void);

/**
 *	Set cursor to (0,0)
 */
void OLED_home(void);

/**
 *	Set cursor to (0, line)
 */
void OLED_goto_line(uint8_t line);

/**
 *	clear given line
 */
void OLED_clear_line(uint8_t line);

/**
 *	Set cursor to (column, row)
 */
void OLED_pos(uint8_t row, uint8_t column);

/**
 *	Write 8 pixels to the OLED at the current cursor position
 */
void OLED_write_data(uint8_t data); //volatile

/**
 *	Write string to the OLED
 */
int OLED_print(char* text);

/**
*	Write number to the OLED
*/
int OLED_print_int(int16_t number, e_alignment alignment);

/**
*	Set font size
*/
void OLED_set_font_size(e_font_size size);

/**
 *	Set brightness to lvl
 */
void OLED_set_brightness(uint8_t lvl);

#endif /* OLED_H_ */