/*
 * hmi.h
 *
 * Created: 16.09.2022 14:46:42
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */ 

#ifndef GAME_H_
#define GAME_H_

/* global constants */
#define NO_CALIBRATION	0
#define DO_CALIBRATION	1

/**
 *	Initializes all HMI periphery
 */
void init_game(uint8_t calibrate_joystick);

/**
 *	Entry point to hmi state machine
 */
void start_game(void);

#endif /* GAME_H_ */