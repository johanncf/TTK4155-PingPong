/*
 * can.c
 *
 * Created: 10/7/2022 11:28:58 AM
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 */

#include "can.h"
#include "mcp2515.h"
#include <avr/interrupt.h>

//#define DEBUG_CAN

/* library global variables */
volatile uint8_t g_new_can_msg;
volatile can_message g_message;

/**
 *	Initializes the CAN controller
 */
void can_init(uint8_t loopback_enable) {
	mcp2515_init();
	
	// set bit timing
	mcp2515_bit_modify(MCP_CNF1, 0x3F, 7);					// set baudrate prescaler to 16 -> 100 kHz TQ
	mcp2515_bit_modify(MCP_CNF2, 0x3F, (0x02<<3 | 0x02));	// use 3 TQ for PRSEG and PHSEG1
	mcp2515_bit_modify(MCP_CNF3, 0x07, 0x02);				// use 3 TQ for PHSEG2
		
	// set interrupt on mcp2515
	mcp2515_bit_modify(MCP_CANINTE, 0xFF, 0x1F);
	mcp2515_bit_modify(MCP_CANINTF, 0xFF, 0x00);
	
	if (loopback_enable) {
		mcp2515_bit_modify(MCP_CANCTRL, 0xE0, MODE_LOOPBACK);
		mcp2515_read(MCP_CANSTAT);
		} else {
		mcp2515_bit_modify(MCP_CANCTRL, 0xE0, MODE_NORMAL);
		mcp2515_read(MCP_CANSTAT);
	}
	
	// configure interrupt
	cli();					// disable global interrupts 
	CAN_DDR &= ~(1<<CAN_INT_PIN);  //setting as input 
	MCUCR |= (1 << ISC01);	// falling edge
	GICR  |= (1 << INT0);	// enable interrupt INT0
	sei();					// enable global interrupts
}


/**
 *	Send a complete CAN message
 */
void can_message_send (can_message *msg) {
	uint8_t i, id_lsb, id_msb;
	id_msb = (uint8_t)(msg->id >> 3);
	id_lsb = (uint8_t)(msg->id << 5);
	mcp2515_bit_modify(MCP_TXB0SIDH, 0xFF, id_msb);
	mcp2515_bit_modify(MCP_TXB0SIDL, 0xE0, id_lsb);
	mcp2515_bit_modify(MCP_TXB0DLC, 0x0F, msg->data_length);
	for (i = 0; i < msg->data_length; i++) {
		mcp2515_bit_modify(MCP_TXB0D0 + i, 0xFF, msg->data[i]);
	}
	mcp2515_request_to_send();
}


/**
 *	Read a CAN message from the CAN controller into the MCU
 *  (only called in ISR)
 */
void can_data_receive(can_message *msg, uint8_t rx_reg) {
	uint8_t i, id_lsb, id_msb, reg_offset, flag_offset;
	reg_offset = (rx_reg == RX_REG0) ? 0 : 0x10;
	flag_offset = (rx_reg == RX_REG0) ? 0 : 0x01;
	id_msb = mcp2515_read(MCP_RXB0SIDH + reg_offset);
	id_lsb = mcp2515_read(MCP_RXB0SIDL + reg_offset);
	msg->id = (id_msb <<3) + ((id_lsb & 0xE0) >> 5);
	msg->data_length = mcp2515_read(MCP_RXB0DLC + reg_offset);
	
	for (i = 0; i < msg->data_length; i++) {
		msg->data[i] = mcp2515_read(MCP_RXB0D0 + i + reg_offset);
	}
	// clear interrupt flag
	mcp2515_bit_modify(MCP_CANINTF, MCP_RX0IF + flag_offset, 0x00);
}


/**
 *	Returns nonzero value if new message is available
 */
uint8_t can_new_message_received(void) {
	if (g_new_can_msg) {
		g_new_can_msg = 0;
		return 1;
	}
	return 0;
}


/**
 *	Returns latest CAN message
 */
can_message can_get_latest_message(void) {
	return g_message;
}


/**
 *	Send an empty CAN message with an ID only
 */
void can_send_id_only(uint16_t id) {
	can_message message;
	message.id = id;
	message.data_length = 0;
	can_message_send(&message);
}


/**
 *	ISR for the CAN controller interrupts
 */
ISR(INT0_vect) {
	uint8_t status_byte;
	
#ifdef DEBUG_CAN 
	printf("Interrupt: ");
#endif /* DEBUG_CAN */
	status_byte = mcp2515_read_status();
	
#ifdef DEBUG_CAN 
	if (status_byte & MCP_TX0IF) {
		printf("Message sent MCP_TX0IF; ");
	}
	if (status_byte & MCP_TX1IF) {
		printf("Message sent MCP_TX1IF; ");
	}
	if (status_byte & MCP_TX2IF) {
		printf("Message sent MCP_TX1IF; ");
	}
#endif /* DEBUG_CAN */

	if (status_byte & MCP_RX0IF) {
		can_data_receive(&g_message, RX_REG0);
#ifdef DEBUG_CAN 
		if (DEBUG_CAN) printf("Message received MCP_RX0IF: %02x, ID: %d; ", g_message.data[0], g_message.id);
#endif /* DEBUG_CAN */
		g_new_can_msg = 1;
	}

	if (status_byte & MCP_RX1IF) {
		can_data_receive(&g_message, RX_REG1);
#ifdef DEBUG_CAN 
		if (DEBUG_CAN) printf("Message received MCP_RX1IF: %02x, ID: %d; ", g_message.data[0], g_message.id);
#endif /* DEBUG_CAN */
		g_new_can_msg = 1;
	}

#ifdef DEBUG_CAN 
	if (DEBUG_CAN) printf("\r\n");
#endif /* DEBUG_CAN */
	
	mcp2515_bit_modify(MCP_CANINTF, 0xFF, 0x00);
}