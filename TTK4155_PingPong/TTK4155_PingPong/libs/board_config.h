/*
 * board_config.h
 *
 * Created: 02.09.2022 13:19:51
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 *
 * This Header file contains constants for the specific board.
 */
#ifndef BOARD_CONFIG_H_
#define BOARD_CONFIG_H_

#define DEMO

// Set configuration for board demonstration
#ifndef DEMO
#define USE_SLIDER_FOR_MOTOR
#endif

// Clock Speed
#define F_CPU 4915200
#include <util/delay.h>


#define SRAM_BASE_ADDRESS	0x1800
#define SRAM_SIZE			0x800

#define ADC_ADDRESS			0x1400

#define OLED_CMD_ADDRESS	0x1000
#define OLED_DATA_ADDRESS	0x1200

#endif /* BOARD_CONFIG_H_ */