/*
 * TTK4155_PingPong.c
 *
 * Created: 01.09.2022 15:01:26
 * Authors: Tanvir Hassan Tusher, Max Laser, Johannes Fahr
 *
 *
 * Cutoff frequency of the LPF: 796 Hz
 */

#include "libs/board_config.h"
#include "libs/uart.h"
#include "libs/xmem.h"
#include "libs/game.h"
#include <stdio.h>
#include <avr/io.h>


/************************************************************************/
/* MAIN                                                                 */
/************************************************************************/
int main(void)
{
    /* initialize serial port with 9600 BAUD */
	uart_init(9600);

	/* initialize ext. SRAM */
	xmem_init();
	
	init_game(NO_CALIBRATION);
	
	start_game();
}
